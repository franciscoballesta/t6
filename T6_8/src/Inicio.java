import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	8) Crea un array de 10 posiciones de n�meros con valores pedidos por teclado. Muestra por
	consola el indice y el valor al que corresponde. Haz dos m�todos, uno para rellenar valores y
	otro para mostrar.
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int num[] = new int[10];
		rellenar(num);
		mostrar(num);
	}
	private static int[] rellenar(int[] numeros) {
		for (int i = 0; i < numeros.length; i++) {
			String strnro = JOptionPane.showInputDialog("Introduce un n�mero " + i);
			int nro = Integer.parseInt(strnro);
			numeros[i]=nro;
		}
		return numeros;
		
	}
	private static void mostrar(int[] numeros) {
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
		}
	}
}
