import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	3) Crea una aplicaci�n que nos pida un n�mero por teclado y con un m�todo se lo pasamos
	por par�metro para que nos indique si es o no un n�mero primo, debe devolver true si es
	primo sino false.
	Un n�mero primo es aquel solo puede dividirse entre 1 y si mismo. Por ejemplo: 25 no es
	primo, ya que 25 es divisible entre 5, sin embargo, 17 si es primo.
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce un n�mero");
		int nro = Integer.parseInt(strnro);
		if(primo(nro)==true)
			System.out.println("Es primo");
		else
			System.out.println("No es primo");
	}
	private static boolean primo(int numero){
		if (numero <= 1) {
            return false;
        }
        int contador = 0;
        for (int i = (int) Math.sqrt(numero); i > 1; i--) {
            if (numero % i == 0) {
                contador++;
            }
        }
        return contador < 1;		
	}
}
