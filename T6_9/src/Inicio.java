import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	9) Crea un array de n�meros donde le indicamos por teclado el tama�o del array,
	rellenaremos el array con n�meros aleatorios entre 0 y 9, al final muestra por pantalla el
	valor de cada posici�n y la suma de todos los valores. Haz un m�todo para rellenar el array
	(que tenga como par�metros los n�meros entre los que tenga que generar), para mostrar el
	contenido y la suma del array y un m�todo privado para generar n�mero aleatorio (lo
	puedes usar para otros ejercicios).
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce una cantidad de n�meros");
		int nro = Integer.parseInt(strnro);
		int numeros[] = new int[nro];
		rellenar(numeros);
		mostrar(numeros);
	}
	private static void rellenar(int[] numeros) {
		int desde = (int)0;
		int hasta = (int)9;
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = Aleatorio(desde,hasta);
		}
	}
	private static void mostrar(int[] numeros) {
		int suma = 0;
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
			suma += numeros[i];
		}
		System.out.println("Suma: " + suma);
	}

	private static int Aleatorio(int desde, int hasta) {
		int rdm=(int)Math.floor(Math.random()*(desde-(hasta+1))+(hasta));
		return rdm + 1;
	}

}
