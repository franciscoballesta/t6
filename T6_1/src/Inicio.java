import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	
	1) Crea una aplicaci�n que nos calcule el �rea de un circulo, cuadrado o triangulo. Pediremos
	que figura queremos calcular su �rea y seg�n lo introducido pedir� los valores necesarios
	para calcular el �rea. Crea un m�todo por cada figura para calcular cada �rea, este devolver�
	un n�mero real. Muestra el resultado por pantalla.
	Aqu� te mostramos que necesita cada figura:
	�
	�
	�
	Circulo: (radio^2)*PI
	Triangulo: (base * altura) / 2
	Cuadrado: lado * lado	 
	*/
public class Inicio {
	
	private static final double PI = 3.1416;
	
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce un n�mero de figura a calcular(1:C�rculo,2:Cuadrado,3:Tri�ngulo");
		int figura = Integer.parseInt(strnro);
		double res = 0;
		switch (figura) {
		case 1:
			String sr = JOptionPane.showInputDialog("Introduce el radio");
			double r = Double.parseDouble(sr);
			res = Circulo(r);
			break;
		case 2:
		case 3:
			String sb = JOptionPane.showInputDialog("Introduce la base");
			double b = Double.parseDouble(sb);
			String sa = JOptionPane.showInputDialog("Introduce la altura");
			double a = Double.parseDouble(sa);
			res = Cuadrado(b,a);
			if(figura==3) res = res / 2;
			break;

		default:
			break;
		}
		JOptionPane.showMessageDialog(null, "Area: " + res);		
	}
	private static double Circulo(double radio) {
		double res = Math.pow(radio, 2) * PI;
		return res;
	}
//	private static double Triangulo(double base, double altura) {
//		double res = (base * altura) / 2;
//		return res;
//	}
	private static double Cuadrado(double ladoA, double ladoB) {
		double res = ladoA * ladoB;
		return res;
	}
}
