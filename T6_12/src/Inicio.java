import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	12) Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
	n�meros aleatorios entre 1 y 300 y mostrar� aquellos n�meros que acaben en un d�gito que
	nosotros le indiquemos por teclado (debes controlar que se introduce un numero correcto),
	estos deben guardarse en un nuevo array.
	Por ejemplo, en un array de 10 posiciones le indicamos mostrar los n�meros acabados en 5,
	podr�a salir 155, 25, etc.
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce una cantidad de n�meros");
		int nro = Integer.parseInt(strnro);
		int numeros[] = new int[nro];
		int desde = 1;
		int hasta = 300;

		rellenar(numeros, desde, hasta);
		strnro = JOptionPane.showInputDialog("Introduce el numero con el que acaben los n�meros a buscar");
		nro = Integer.parseInt(strnro);
		for (int i = 0; i < numeros.length; i++) {
			String snum = String.valueOf(numeros[i]);
			if(snum.endsWith(strnro)){
				System.out.println(numeros[i]);
			}
		}

	}
	private static void rellenar(int[] numeros,int desde,int hasta) {
		for (int i = 0; i < numeros.length; i++) {
			int ale = 0;
			ale = Aleatorio(desde,hasta);
			numeros[i] = ale;
		}
	}
	private static int Aleatorio(int desde, int hasta) {
		int rdm=(int)Math.floor(Math.random()*(desde-(hasta+1))+(hasta));
		return rdm + 1;
	}

}
