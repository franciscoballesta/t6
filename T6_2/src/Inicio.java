import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	2) Crea una aplicaci�n que nos genere una cantidad de n�meros enteros aleatorios que
	nosotros le pasaremos por teclado. Crea un m�todo donde pasamos como par�metros entre
	que n�meros queremos que los genere, podemos pedirlas por teclado antes de generar los
	n�meros. Este m�todo devolver� un n�mero entero aleatorio. Muestra estos n�meros por
	pantalla.
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce una cantidad de n�meros");
		int nro = Integer.parseInt(strnro);
		int numeros[] = new int[nro];
		String strdesde = JOptionPane.showInputDialog("Introduce un limite inferior");
		int desde = Integer.parseInt(strdesde);
		String strhasta = JOptionPane.showInputDialog("Introduce un limite superior");
		int hasta = Integer.parseInt(strhasta);
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = Aleatorio(desde,hasta);
		}
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);;
		}
		
		

	}
	private static int Aleatorio(int desde, int hasta) {
		int rdm=(int)Math.floor(Math.random()*(desde-(hasta+1))+(hasta));
		return rdm + 1;
	}

}
