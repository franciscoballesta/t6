import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	11) Crea dos arrays de n�meros con una posici�n pasado por teclado.
	Uno de ellos estar� rellenado con n�meros aleatorios y el otro apuntara al array anterior,
	despu�s crea un nuevo array con el primer array (usa de nuevo new con el primer array) con
	el mismo tama�o que se ha pasado por teclado, rellenalo de nuevo con n�meros aleatorios.
	Despu�s, crea un m�todo que tenga como par�metros, los dos arrays y devuelva uno nuevo
	con la multiplicaci�n de la posici�n 0 del array1 con el del array2 y as� sucesivamente. Por
	�ltimo, muestra el contenido de cada array. 
*/
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce una cantidad de n�meros");
		int nro = Integer.parseInt(strnro);
		int numeros1[] = new int[nro];
		int numeros2[] = new int[nro];
		rellenar(numeros1, 0, 9);
		rellenar(numeros2, 0, 9);
		int[] producto = multiplicar(numeros1, numeros2);
		for (int i = 0; i < producto.length; i++) {
			int j = producto[i];
			System.out.println(i + ": " + numeros1[i] + " * " + numeros2[i] + " = " + producto[i]);
		}
	}
	private static void rellenar(int[] numeros,int desde,int hasta) {
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = Aleatorio(desde,hasta);
		}
	}
	private static int Aleatorio(int desde, int hasta) {
		int rdm=(int)Math.floor(Math.random()*(desde-(hasta+1))+(hasta));
		return rdm + 1;
	}
	private static int[] multiplicar(int[] numeros1,int[] numeros2) {
		int mul[] = new int[numeros1.length];
		for (int i = 0; i < numeros1.length; i++) {
			mul[i] = numeros1[i] * numeros2[i];
		}
		return mul;
	}

}
