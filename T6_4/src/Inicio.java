import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	4) Crea una aplicaci�n que nos calcule el factorial de un n�mero pedido por teclado, lo
	realizara mediante un m�todo al que le pasamos el n�mero como par�metro. Para calcular
	el factorial, se multiplica los n�meros anteriores hasta llegar a uno. Por ejemplo, si
	introducimos un 5, realizara esta operaci�n 5*4*3*2*1=120.
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce un n�mero");
		double nro = Double.parseDouble(strnro);
		for (double i = nro-1; i > 0; i--) {
			nro = nro * i;
		}
		System.out.println("Factorial: " + nro);
	}

}
