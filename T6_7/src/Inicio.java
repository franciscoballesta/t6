import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	7) Crea un aplicaci�n que nos convierta una cantidad de euros introducida por teclado a otra
	moneda, estas pueden ser a dolares, yenes o libras. El m�todo tendr� como par�metros, la
	cantidad de euros y la moneda a pasar que sera una cadena, este no devolver� ning�n valor,
	mostrara un mensaje indicando el cambio (void).
	El cambio de divisas son:
	�
	�
	�
	0.86 libras es un 1 �
	1.28611 $ es un 1 �
	129.852 yenes es un 1 �
 */
public class Inicio {

	/**
	 * @param args
	 */
	private static final double LIBRA = 0.86;
	private static final double YEN = 129.852;
	private static final double DOLAR = 1.28611;
	
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce una cantidad de euros");
		double nro = Double.parseDouble(strnro);
		strnro = JOptionPane.showInputDialog("Introduce divisa (l:libras,d:dolares,y:yenes)");
		cambio(nro,strnro);

	}
	private static void cambio(double euros, String moneda) {
		double res = 0;
		switch (moneda) {
		case "l":
			res = euros * LIBRA;
			break;
		case "d":
			res = euros * DOLAR;
			break;
		case "y":
			res = euros * YEN;
			break;
		default:
			break;
		}
		System.out.println("Valor: " + res);
	}
}
