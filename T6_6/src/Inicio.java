import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	6) Crea una aplicaci�n que nos cuente el n�mero de cifras de un n�mero entero positivo
	(hay que controlarlo) pedido por teclado. Crea un m�todo que realice esta acci�n, pasando
	el n�mero por par�metro, devolver� el n�mero de cifras.
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce un n�mero");
		int nro = Integer.parseInt(strnro);
		if (nro>0) 
			System.out.println("Nro de car�cteres: " + strnro.length());
		else
			System.out.println("No v�lido");

	}

}
