import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	10) Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
	n�meros aleatorios primos entre los n�meros deseados, por �ltimo nos indicar cual es el
	mayor de todos.
	Haz un m�todo para comprobar que el n�mero aleatorio es primo, puedes hacer todos lo
	m�todos que necesites.
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce una cantidad de n�meros");
		int nro = Integer.parseInt(strnro);
		int numeros[] = new int[nro];
		String strdesde = JOptionPane.showInputDialog("Introduce un limite inferior");
		int desde = Integer.parseInt(strdesde);
		String strhasta = JOptionPane.showInputDialog("Introduce un limite superior");
		int hasta = Integer.parseInt(strhasta);

		rellenar(numeros, desde, hasta);
		mayor(numeros);
	}
	private static void mayor(int[] numeros) {
		int may = 0;
		for (int i = 0; i < numeros.length; i++) {
			if(numeros[i]>may) may = numeros[i]; 
		}
		System.out.println(may);
	}
	private static void rellenar(int[] numeros,int desde,int hasta) {
		for (int i = 0; i < numeros.length; i++) {
			int ale = 0;
			//cuidado
			do {
				ale = Aleatorio(desde,hasta);
			} while (!primo(ale));
			numeros[i] = ale;
		}
	}
	private static int Aleatorio(int desde, int hasta) {
		int rdm=(int)Math.floor(Math.random()*(desde-(hasta+1))+(hasta));
		return rdm + 1;
	}
	private static boolean primo(int numero){
		if (numero <= 1) {
            return false;
        }
        int contador = 0;
        for (int i = (int) Math.sqrt(numero); i > 1; i--) {
            if (numero % i == 0) {
                contador++;
            }
        }
        return contador < 1;		
	}

}
