import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
	5) Crea una aplicaci�n que nos convierta un n�mero en base decimal a binario. Esto lo
	realizara un m�todo al que le pasaremos el numero como par�metro, devolver� un String
	con el numero convertido a binario. Para convertir un numero decimal a binario, debemos
	dividir entre 2 el numero y el resultado de esa divisi�n se divide entre 2 de nuevo hasta que
	no se pueda dividir mas, el resto que obtengamos de cada divisi�n formara el numero
	binario, de abajo a arriba.
 */
public class Inicio {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String strnro = JOptionPane.showInputDialog("Introduce un n�mero");
		int nro = Integer.parseInt(strnro);
		String res = "";
		if (nro <= 0) {
			res =  "0";
		}
		while (nro > 0) {
			int resto = (int) (nro % 2);
			nro = nro / 2;
			res = resto + res;
		}
		System.out.println(res);
	}

}
